import { Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';

export default function Login() {
  

  // State hooks to store the values of the input fields
  const [ email, setEmail ] = useState('');
  const [ Password, setPassword ] = useState('');
  // State to determine whether submit button is enabled or not
  const [ isActive, setIsActive] = useState(false);

  useEffect(() => {


    // Validation to enable submit buttion when all fields are populated and both paswords match.
    if (email !== '' && Password !== '') {
      setIsActive(true);
    } else {

      setIsActive(false);
    }
  })

  function registerUser(e) {
    e.preventDefault();

    // MiniActivity
    // Write a code that empty the input field for email, password1, password2

    setPassword('');
    setEmail('');
    alert("You are logged in!");
  }

  return (

    <Form onSubmit={(e) => registerUser(e)}>

      <Form.Text className="text-dark fs-1 fw-bold">
          Login
      </Form.Text>

      <Form.Group controlId="email">
        <Form.Label>Email Address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={e => setEmail(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={Password}
          onChange={e => setPassword(e.target.value)}
          required
        />
      </Form.Group>

      { isActive ?
        <Button variant="success" type='submit' id=' submitBtn'> 
          Login
        </Button>
        :
        <Button variant="danger" type='submit' id=' submitBtn' disabled> 
          Login
        </Button>
      }
    </Form>
  )
}
















