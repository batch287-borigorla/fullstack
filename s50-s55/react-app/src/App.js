import AppNavbar from './components/AppNavbar';
// import Home from './pages/Home';
// import Courses from './pages/Courses';
// import Register from './pages/Register';
import Login from './pages/Login';
import './App.css';

import { Container } from 'react-bootstrap';

function App() {
  return (
    // A common pattern in React for the components to treturn multiple elements
    <>
    <AppNavbar />
    <Container>
       {/*<Home />*/}
       {/*<Courses />*/}
      {/*<Register />*/}
      <Login />
    </Container>
    </>
  );
}
 
export default App;
